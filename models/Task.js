const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
			name: {
				type: String,
				required: [true, "Task name is required"]
			},
			status: {
				type: String,
				default: "pending"
			}
		});

// module.exports will allow us to exports files/functions and be able to import/require them in another file within our application.
// files/functions of Task.js will be used in the controllers folder.
module.exports = mongoose.model("Task", taskSchema)