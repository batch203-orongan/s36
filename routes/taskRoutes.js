const express = require("express");
// express.Router() mehod allows access to HTTP methods.
const router = express.Router();

const taskControllers =require("../controllers/taskControllers");

console.log(taskControllers);

// Create task routes
router.post("/", taskControllers.createTaskControllers);

// View all tasks routes
router.get("/allTasks", taskControllers.getAllTasksContoller);

// Get a single task
router.get("/getSingleTask/:taskId", taskControllers.getSingleTaskController);

// Update a task status
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);

// Delete a task
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

// this will be used in our server.
module.exports = router;

